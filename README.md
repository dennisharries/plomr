
<!-- README.md is generated from README.Rmd. Please edit that file -->

# plomr

<!-- badges: start -->

<!-- badges: end -->

plomr provides basic functions to calculate interference colors arising
from polarized light passing through an anisotropic, birefringent
material between crossed polarizers. This is currently being developed
to address effects of anomalous birefringence due to wavelength
dispersion of refractive indices and birefringence. The current state
has not been tested against empirical evidence.

## Installation

You can install the released version of plomr with:

``` r
library(devtools)
devtools::install_gitlab("dennisharries/plomr")
```

## Example

Simple example comparing normal and anomalous interference colors:

``` r
library(plomr)

d_seq = seq(0, 500, by = 1)

image(
  x = d_seq,
  y = c(1, 2),
  z = matrix(seq(1, 2 * length(d_seq)), ncol = 2),
  xlab = "Thickness (µm)",
  yaxt = "n",
  ylab = "",

  col = c(
    calcRGBseqDisp(
      thickness_seq = d_seq,
      delta_ref = 0.010,
      disp = 0,
      lambda_step = 1), # bottom

    calcRGBseqDisp(
      thickness_seq = d_seq,
      delta_ref = 0.010,
      disp = -0.00001,
      lambda_step = 1) # top
  )
)

axis(2, at = c(1, 2), labels = c("Normal", "Anomalous"))
```

<img src="man/figures/README-example-1.png" width="100%" />
